package dao;

import org.springframework.data.jpa.repository.JpaRepository;


import model.Playerdetails;

public interface PlayerdetailsRepo extends JpaRepository<Playerdetails, Integer> {//Entity class name, Identifier type


}
