package dao;

import org.springframework.data.jpa.repository.JpaRepository;


import model.Sports;

public interface SportsRepo extends JpaRepository<Sports, Integer> {//Entity class name, Identifier type


}
