package dao;

import org.springframework.data.jpa.repository.JpaRepository;


import model.Signup;

public interface SignupRepo extends JpaRepository<Signup, Integer> {//Entity class name, Identifier type

}
