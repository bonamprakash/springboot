package dao;

import org.springframework.data.jpa.repository.JpaRepository;


import model.Queries;

public interface QueriesRepo extends JpaRepository<Queries, Integer> {//Entity class name, Identifier type

}
