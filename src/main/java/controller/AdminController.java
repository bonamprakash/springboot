package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import dao.PlayerdetailsRepo;
import model.Playerdetails;

@RestController
@CrossOrigin(origins ="http://localhost:4200") //it permits all the requests from http:localhost:4200
public class AdminController {

   @Autowired
   private PlayerdetailsRepo Playerdetails;
  
	@GetMapping("viewAllPlayers")
	   public List<Playerdetails> viewAllPlayers() {
	       return Playerdetails.findAll();
	   }
	  @PostMapping("addPlayers")
	  public String addPlayers(@RequestBody Playerdetails s) {
		  Playerdetails.saveAndFlush(s);
	      return "Added Successfully";
	  }

	  @PutMapping("adminupdate")
	  public String adminupdate(@RequestBody Playerdetails s) {
		  Playerdetails.findById(s.getId()).map(sp -> {
	          sp.setId(s.getId());
	          sp.setName(s.getName());
	          sp.setGamename(s.getGamename());
	          sp.setAge(s.getAge());
	          return Playerdetails.save(s);
	      });
	      return "Updated";
	 }
	  
	  @DeleteMapping("admindelete/{id}")
	   public String admindelete(@PathVariable("id") int id) {
		  Playerdetails.deleteById(id);
	       return "Deleted";
	   }
	  
	  @GetMapping("adminsearch/{id}")
	  public String adminsearch(@PathVariable("id") int id) {
	      return Playerdetails.findById(id).get().getName();
	  }
	   }
