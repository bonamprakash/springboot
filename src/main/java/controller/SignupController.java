package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import dao.SignupRepo;
import model.Signup;

@RestController
public class SignupController {

   @Autowired
   SignupRepo Signup;

   @PostMapping("signup")
   public String signup(@RequestBody Signup s) {
	   Signup.saveAndFlush(s);
       return "user created Successfully";
   }
   

}