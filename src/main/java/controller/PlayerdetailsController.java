package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import dao.PlayerdetailsRepo;
import dao.QueriesRepo;
import model.Playerdetails;
import model.Queries;

@RestController
@CrossOrigin(origins ="http:localhost:4200") //it permits all the requests from http:localhost:4200
public class PlayerdetailsController {

   @Autowired
    PlayerdetailsRepo Playerdetails;
  QueriesRepo Signup;


   @GetMapping("viewPlayers")
    public List<Playerdetails> findAll() {
        return Playerdetails.findAll();
    }
   /*@PostMapping("addPlayers")
   public String add(@RequestBody Playerdetails s) {
	   Playerdetails.saveAndFlush(s);
       return "Added Successfully";
   }
*/
   @PutMapping("update")
   public String update(@RequestBody Playerdetails s) {
	   Playerdetails.findById(s.getId()).map(sp -> {
           sp.setId(s.getId());
           sp.setName(s.getName());
           sp.setGamename(s.getGamename());
           sp.setAge(s.getAge());
           return Playerdetails.save(s);
       });
       return "Data Updated";
  }
   
   @DeleteMapping("delete/{id}")
    public String delete(@PathVariable("id") int id) {
	   Playerdetails.deleteById(id);
        return "Data Deleted";
    }
      @GetMapping("search/{id}")
   public String search(@PathVariable("id") int id) {
       return Playerdetails.findById(id).get().getName();
   }
//     @PostMapping("queries")
//      public String queries(@RequestBody Queries q) {
//    	 Queries.saveAndFlush(q);
//          return "user created Successfully";
//      }
   
}