package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import dao.SignupRepo;
import dao.SportsRepo;

import model.Sports;

@RestController
public class SportsController {

   @Autowired
    SportsRepo Sports;
   SignupRepo Signup;

   @GetMapping("view")
    public List<Sports> findAll() {
        return Sports.findAll();
    }
   @PostMapping("add")
   public String add(@RequestBody Sports s) {
	   Sports.saveAndFlush(s);
       return "Added Successfully";
   }

   @PutMapping("update")
   public String update(@RequestBody Sports s) {
	   Sports.findById(s.getId()).map(sp -> {
           sp.setId(s.getId());
           sp.setUsername(s.getUsername());
           sp.setGamename(s.getGamename());
           sp.setAge(s.getAge());
           return Sports.save(s);
       });
       return "Data Updated";
  }
   
   @DeleteMapping("delete/{id}")
    public String delete(@PathVariable("id") int id) {
	   Sports.deleteById(id);
        return "Data Deleted";
    }
      @GetMapping("search/{id}")
   public String search(@PathVariable("id") int id) {
       return Sports.findById(id).get().getUsername();
   }
   /*@PostMapping("signup")
   public String signup(@RequestBody model.Signup s) {
	   Signup.saveAndFlush(s);
       return "user created Successfully";
   }*/
   
}