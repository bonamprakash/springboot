package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import dao.QueriesRepo;
import model.Queries;

@RestController
@CrossOrigin(origins ="http:localhost:4200") //it permits all the requests from http:localhost:4200
public class QueriesController {

   @Autowired
  QueriesRepo Queries;


 
     @PostMapping("queries")
      public String queries(@RequestBody Queries q) {
    	 Queries.saveAndFlush(q);
          return "user created Successfully";
      }
   
}